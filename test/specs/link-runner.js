import faker from "faker"

describe('test link runer for skoda', function() {
    // массив доступных моделей автомобиля  
    const modelCar = [
        'kodiaq',
        'yeti',
        'octavia',
        'rapid'
    ];
    it('go to new page', () => {
        // указать сколько раз нужно програнь тест
        const repetitions = 3;
        for(let i = 1; i <= repetitions; i++ ) {
                console.log(i);
            // сгенерируем новый id 
            const id = faker.random.number(10000000000000000);
            // сгенерируем рандомную модель автомобиля
            const rand = Math.floor(Math.random() * modelCar.length);
            // вынесем сслыку для красоты
            const link = `http://www.skoda-avto.ru/welcome-page?utm_source=yandex&utm_medium=email&utm_campaign=test_purchaser&intID=test_${id}&model=${modelCar[rand]}`;
            // переходим на страницу 
            browser.url(link);
            // ожидаем закрузки всех скриптом, по самому тяжёлому файлу в нетворке
            browser.pause(3000);
            browser.deleteCookie();
        }
    });
});